#!/usr/bin/env bash

##Creating cache files
mkdir /tmp/arminho-cache
cache="/tmp/arminho-cache"

	#Main folders
mkdir $cache/downloads
mkdir $cache/user
mkdir $cache/user/telegram
mkdir $cache/scripts

clear
###User choices

echo -e "Welcome to Telegram Desktop Portable installer for Linux (unnoficial)\n\nPlease, select the desired version:\n1 - Install Telegram in portable mode \n2 - Install Telegram in default mode"
read telegram && echo "$telegram" > $cache/user/telegram/version

while [ $telegram != "1" -a $telegram != "2" ]
do
    echo 'Plase type again'
    read telegram && echo "$telegram" > $cache/user/telegram/version
done

zenity --title "Select the installation folder" --file-selection --directory > $cache/user/telegram/dir

##Proceed to installation
echo -e "\n\nProceed to installation?\n\n1 - Yes\n0 - No"
read install && echo "$install" > $cache/user/telegram/install

while [ $install != "1" -a $install != "0" ]
do
    echo 'Plase type again'
    read install && echo "$install" > $cache/user/telegram/install
done

if [ $install = "1" ]
then
    wget -P $cache/scripts https://gitlab.com/SrArminho/telegram-desktop-potable-for-linux/-/raw/main/conf/install.sh
    chmod +x $cache/scripts/install.sh
    $cache/scripts/install.sh
fi

##Done
clear
echo "All Done!"
read
