#!/usr/bin/env bash

# This script will reinstall Telegram binary

cd ..
mkdir /tmp/arminho-cache
cache="/tmp/arminho-cache"
mkdir $cache/downloads
mkdir $cache/user
mkdir $cache/user/telegram
mkdir $cache/user/telegram/fix

pwd > $cache/user/telegram/fix/dir
dir=`cat $cache/user/telegram/fix/dir`


## Download files

wget -P $cache/downloads https://telegram.org/dl/desktop/linux


## Extract Files

mv $cache/downloads/linux $cache/downloads/tsetup.tar.xz
tar -xf $cache/downloads/tsetup.tar.xz -C $cache/downloads

## Move files

mv $cache/downloads/Telegram/Telegram "$dir"
mv $cache/downloads/Telegram/Updater "$dir"

## Finish

clear
echo -e "All done!\n\nTry now"
read
