## This script will fix AppRun.sh


cd ..
mkdir /tmp/arminho-cache
cache="/tmp/arminho-cache"
mkdir $cache/downloads
mkdir $cache/user
mkdir $cache/user/telegram
mkdir $cache/user/telegram/fix

pwd > $cache/user/telegram/fix/dir
dir=`cat $cache/user/telegram/fix/dir`

## Donwload AppRun.sh from GitLab

wget -P $cache/downloads https://gitlab.com/SrArminho/telegram-desktop-potable-for-linux/-/raw/main/conf/AppRun.sh

## Allow permissions

chmod +x $cache/downloads/AppRun.sh

## Move AppRun.sh

mv $cache/downloads/AppRun.sh "$dir/AppRun.sh"

## Finish

clear
echo -e "All done!\n\nTry now"
read
