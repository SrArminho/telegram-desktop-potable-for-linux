## This script will fix updates of Telegram

cd ..

dir=`pwd`

mv "$dir/tdata/tupdates/temp/Telegram" "$dir"
mv "$dir/tdata/tupdates/temp/Updater" "$dir"
rm -Rf "$dir/tdata/tupdates"

## Finish

clear
echo -e "All done!\n\nTry now"
read
