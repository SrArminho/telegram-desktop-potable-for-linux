## Hello friend, this is my first shell script project. Feel free to see how it works at https://gitlab.com/SrArminho/telegram-desktop-potable-for-linux


### This folder contains scripts to fix possible problems



Maybe Telegram stops working through AppRun.sh after an update, you can use Telegram by directly clicking on the "Telegram" file but your data will not be saved in the "tdata" folder

To fix this you can try running "fix-update.sh"

If that doesn't work, you can reinstall Telegram with "reinstall-telegram-binary.sh"

If it still doesn't work, try running "fix-apprun.sh"

As a last alternative, you can delete all content inside the "tdata" folder
Remember that this will erase all Telegram data, you will need to login to your account again.
