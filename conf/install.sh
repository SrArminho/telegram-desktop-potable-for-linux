#!/usr/bin/env bash

##Telegram installer

cache="/tmp/arminho-cache"
install=`cat "$cache/user/telegram/install"`
version=`cat "$cache/user/telegram/version"`
dir=`cat "$cache/user/telegram/dir"`

## Download Files
wget -P $cache/downloads https://telegram.org/dl/desktop/linux

## Extract Files
mv $cache/downloads/linux $cache/downloads/tsetup.tar.xz
tar -xf $cache/downloads/tsetup.tar.xz -C $cache/downloads

## Install
if [ $install = "1" ]
then 
    if [ $version = "2" ]
    then
        mkdir $HOME/Applications
        mv $cache/downloads/Telegram "$dir/Telegram"
    fi
    if [ $version = "1" ]
    then
        mkdir $cache/downloads/Telegram/tdata
        mkdir $cache/downloads/Telegram/scripts
        wget -P $cache/downloads/Telegram https://gitlab.com/SrArminho/telegram-desktop-potable-for-linux/-/raw/main/conf/AppRun.sh
        wget -P $cache/downloads/Telegram/scripts https://gitlab.com/SrArminho/telegram-desktop-potable-for-linux/-/raw/main/conf/fix/fix-apprun.sh
        wget -P $cache/downloads/Telegram/scripts https://gitlab.com/SrArminho/telegram-desktop-potable-for-linux/-/raw/main/conf/fix/fix-update.sh
        wget -P $cache/downloads/Telegram/scripts https://gitlab.com/SrArminho/telegram-desktop-potable-for-linux/-/raw/main/conf/fix/reinstall-telegram-binary.sh
        wget -P $cache/downloads/Telegram/scripts https://gitlab.com/SrArminho/telegram-desktop-potable-for-linux/-/raw/main/conf/fix/README.txt
        chmod +x $cache/downloads/Telegram/scripts/fix-apprun.sh
        chmod +x $cache/downloads/Telegram/scripts/fix-update.sh 
        chmod +x $cache/downloads/Telegram/scripts/reinstall-telegram-binary.sh
        chmod +x $cache/downloads/Telegram/AppRun.sh
        mv $cache/downloads/Telegram "$dir/Telegram"
    fi
fi 
