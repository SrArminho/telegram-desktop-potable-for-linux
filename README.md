# Telegram Desktop Portable for Linux 

Officially Telegram does not have a portable version for Linux, with this small project I made Telegram Portable Version for Linux!

## Use cases

If you need help solving a problem and you are using a live distro or just want to carry Telegram with you without leaving any traces on the host PC.
Well... there are many cases where this is useful.

## How to use

Once installed, you just need to run AppRun.sh for it to work

![](images/gif.gif)

## Features

* Automatic updates
* Install the default version (not portable)
* Simple folder selection

## Installation

Download [setup.sh](https://gitlab.com/SrArminho/telegram-desktop-potable-for-linux/-/raw/main/setup.sh?inline=false)

---

Remember to allow execution

```
chmod +x setup.sh
```
---

Execute

```
./setup.sh
```
---

Follow the instructions in the terminal

![](images/Captura_de_tela_de_2023-03-24_16-34-30.png)

---

## FAQ

**Can I put it on a pendrive?**
* Yes. But remember that this is not compatible with FAT32 file systems. I recommend format your pendrive in ext4, btrfs or ntfs

---

**Telegram slow or crashing frequently in USB stick or SD card**

* If you are going to use it on an SD card or a USB stick, I recommend disabling all automatic downloads in the Telegram settings. This will improve performance

--- 

**Is my data safe?**

* All Telegram data, cache and login information are stored in the "tdata" folder. If someone unauthorized accesses your USB stick, they will be able to access your account. It's a security risk.
If Telegram is installed in standard mode, the security of your data will only depend on the security of your operatinal system. 

# Possible problems

### **If you can't install**

* Before trying again, clear the installer cache:
```
rm -Rf /tmp/arminho-cache
```

###

* Make sure you have tar and zenity installed:


For Debian/Ubuntu and derivates
```
sudo apt install tar zenity
```

For Arch Linux/Manjaro and others
```
sudo pacman -S tar zenity
```

For Fedora
```
sudo dnf install tar zenity
```

--- 

### If you can't start Telegram
Maybe Telegram stops working through AppRun.sh after an update, you can use Telegram by directly clicking on the "Telegram" file but your data will not be saved in the "tdata" folder


To fix this enter the "scripts" folder and you can try running "fix-update.sh"

If that doesn't work, you can reinstall Telegram with "reinstall-telegram-binary.sh"

If it still doesn't work, try running "fix-apprun.sh"

As a last alternative, you can delete all content inside the "tdata" folder
Remember that this will erase all Telegram data, you will need to login to your account again.
